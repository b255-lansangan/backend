console.log("Hello World!");


function addNum(num1, num2){
	let sum = num1 + num2;
	console.log("Display sum of " + num1 + " and " + num2);
	console.log(sum);
}
addNum(15, 5);


function subNum(sub1, sub2){
	let difference = sub1 - sub2;
	console.log("Display difference of " + sub1 + " and " + sub2);
	console.log(difference);
}
subNum(20, 5);


/*let multiplier = 10;
let multiplicand = 50;
let product = multiplicand * multiplier;
console.log("The product of " + multiplicand + " and " + multiplier + ":");
console.log(product);


let divident = 50;
let divisor = 10;
let quotient = divident / divisor;
console.log("The quotient of " + divident + " and " + divisor + ":");
console.log(quotient);


*/

function multiplyNum(mul1, mul2){
	let product1 = mul1 * mul2;
	console.log("Display product of " + mul1 + " and " + mul2);
	return product1;
}

let product = multiplyNum(50, 10)
console.log(product);

function divideNum(div1, div2){
	let quotient1 = div1 / div2;
	console.log("Display quotient of " + div1 + " and " + div2);
	return quotient1;
}

let quotient = divideNum(50, 10);
console.log(quotient);

function getCircleArea(pi, radius) {
  
  let myarea = pi * radius ** 2;
  console.log("The result of getting the area of a circle with " + radius + " radius:")
  return myarea;
}

let area = getCircleArea(3.1416, 15);
console.log(area);

function getAverage(av1,av2,av3,av4){
	
	let myaverage = (av1+av2+av3+av4) / 4;
	console.log("The average of " + av1 + "," + av2 + "," + av3 + " and " + av4);
	return myaverage;
}
 
let average = getAverage(20, 40, 60, 80);
console.log(average);


function checkIfpassed(score1,score2){
	let myscore = (score1 / score2) * 100;
	console.log("Is " + score1 + "/" + score2 + " a passing score?")
	return myscore;
}

let score = checkIfpassed(38, 50);
let isPassed = score >= 75;
console.log(isPassed);


/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
