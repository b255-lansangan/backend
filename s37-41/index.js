const express = require("express");
const mongoose = require("mongoose");
// Creates an "app" variable that stores the result of the "express" function that intializes our express application
const app = express();
const cors = require("cors");
// Allows access to routes defined within our application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

// Connect to our mongoDB database
mongoose.connect("mongodb+srv://edward-255:admin123@zuitt-bootcamp.gvevrth.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true

	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
// Defines the "/course" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoutes);



// If(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly
// else if it's needed to be imported it will not run the app and instead export it to be used in another file

if(require.main === module){
	// Will used the defined port number for the application whenever an environment variable is available or will use port 4000 if none is defined
	// This syntax will allow flexibility when using the application locally or as a hosted applicaton
	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	})
}



module.exports = app;