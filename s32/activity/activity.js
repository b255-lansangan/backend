let http = require('http');


http.createServer(function(request, response){
	if(request.url == '/' && request.method == 'GET'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to booking system');

	}

	if(request.url == '/profile' && request.method == 'GET'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to your profile');

	}

	if(request.url == '/courses' && request.method == 'GET'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end(`Here's our courses available`);

	}

		if(request.url == '/addCourse' && request.method == 'POST'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Add course to our resources');

	}

	if(request.url == '/updateCourse' && request.method == 'PUT'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Update a course to our resources');

	}

	if(request.url == '/archiveCourse' && request.method == 'DELETE'){
		// Sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end();

	}

}).listen(4000);

console.log('Server running at localhost:4000');
