fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titlesArray = data.map(todo => {
    	return todo.title;
    });
    console.log(titlesArray);
 });



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
	}

})

.then((response) => response.json())
.then((data) => console.log(data));



fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Created To Do List Item',
		completed:false,
		userId:1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId:1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		dateCompleted: '07-09-21',
		status: 'Complete',
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',
})
.then((response)=>response.json())
.then((data)=>console.log(data))
