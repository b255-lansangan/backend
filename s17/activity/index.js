/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

	function printUserInfo() {
	  let userInfo = {
	    fullName: "John Doe",
	    age: "25",
	    address: {
	      street: "123",
	      city: "Quezon City"
	    },
	    cat: "Joe",
	    dog: "Danny"
	  };
	  
	  console.log("Hello, " + "I'm " + userInfo.fullName);
	  console.log("I am " + userInfo.age + " years old.");
	  console.log("I live in " + userInfo.address.street + ", " + userInfo.address.city);
	  console.log("I have a cat named " + userInfo.cat);
	  console.log("I have a dog named " + userInfo.dog);
	}

	printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

	function printFiveBands() {
	  let fiveBands = ["The Beatles", "Taylor Swift", "The Eagles", "Rivermaya", "Eraserheads"];
	  console.log(fiveBands[0]);
	  console.log(fiveBands[1]);
	  console.log(fiveBands[2]);
	  console.log(fiveBands[3]);
	  console.log(fiveBands[4]);
	}

	printFiveBands();

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

	function printFiveMovies() {
	  let fiveMovies = ["Lion King", "Howl's Moving Castle", "Meet the Robinsons", "School of Rock", "Spirited Away"];
	  console.log(fiveMovies[0]);
	  console.log(fiveMovies[1]);
	  console.log(fiveMovies[2]);
	  console.log(fiveMovies[3]);
	  console.log(fiveMovies[4]);
	}

	printFiveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/


function printFriends(){
	let friendOne = "Eugene"; 
	let friendTwo = "Dennis"; 
	let friendThree = "Vincent";

	console.log("These are my friends:");
	console.log(friendOne); 
	console.log(friendTwo); 
	console.log(friendThree); 
};

printFriends();







//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
