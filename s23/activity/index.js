console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals
let trainer = {};
// Initialize/add the given object properties and methods
trainer.name =  "Ash Ketchum";
trainer.age =  10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	hoenn: ["May", "Max"],
	kanto: ["Brock", "Misty"]
}; 
// Properties
console.log(trainer);
// Methods
trainer.talk = function(){
	return 'Pikachu I choose you!'
}
// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
console.log('Result of dot notation')
console.log(trainer.name)
// Access object properties using square bracket notation
console.log('Result of bracket notation')
console.log(trainer['pokemon'])
// Access the trainer "talk" method
console.log('Result of talk method')
console.log(trainer.talk())


// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target)
	{
		console.log(this.name + ' tackled ' + target.name)
		let myDamage = target.health - this.attack
		console.log(target.name + "'s health is reduced to " + myDamage);
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

// Create/instantiate a new pokemon

let myPokemon = new Pokemon('Pikachu', 12, 12, 12)
// Create/instantiate a new pokemon
let opponentPokemon = new Pokemon('Goedude', 8, 8, 8)

// Create/instantiate a new pokemon
let villain = new Pokemon('Mewtwo', 100, 100, 100)

// Invoke the tackle method and target a different object
opponentPokemon.tackle(myPokemon);

// Invoke the tackle method and target a different object

villain.tackle(opponentPokemon);
opponentPokemon.faint()






//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
