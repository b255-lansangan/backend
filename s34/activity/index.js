const express = require("express")
const app = express()
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
});
let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	// if contents of the "request.body" with the property "userName" and "password" is not empty
	if(req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.userName} successfully registered`);
	} else {
		res.send(`Please input BOTH username and password`)
	}
})

app.get("/users", (req, res) => {
	res.send(users)
});




app.delete("/delete-users", (req, res) => {
  let message;

  if (users.length === 0) {
    message = "No users found";
  } else {
    for (let i = 0; i < users.length; i++) {
      if (req.body.userName === users[i].userName) {
        users.splice(i, 1);
        message = `User ${req.body.userName} has been deleted`;
        break;
      }
    }

   
  }

  res.send(message);
});





















if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;