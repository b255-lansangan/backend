db.users.find(
    { $or: [{ firstName: { $regex: "N" } }, { firstName: { $regex: "S" } }] },
    { firstName: 1, lastName: 1, _id: 0 }
)

db.users.find(
    { $and: [{ company: "none" }, { age: { $gte: 70 } }] }
)

db.users.find(
    { $and: [{ firstName: {$regex: "e", $options: "$i"}}, { age: { $lte: 30 } }] }
)