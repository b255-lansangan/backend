db.fruits.aggregate([
    { 
        $match: { onSale: true } 
    },
    { 
        $count: "true"
    }
]);


db.fruits.aggregate([
    { 
        $match: { stock: { $gte: 20} } 
    },
    { 
        $count: "enoughStock"
    }
]);

db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    { 
        $group: 
        {
            _id: "$supplier_id",
            avg_price: { $avg: { $multiply: ["$price", "$supplier_id"]}}
        } 
    }
]);


db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    { 
        $group: 
        {
            _id: "$supplier_id",
            max_price: { $max: "$price" }
        } 
    }
]);
